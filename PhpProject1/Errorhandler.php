<?php
error_reporting(0); 
register_shutdown_function(shutdown_function);
function shutdown_function()
{
    $last_error = error_get_last();
    if($last_error["type"]==E_ERROR)
    {
       custom_warning_handler(E_ERROR,$last_error["message"],$last_error["file"],$last_error["line"]);
    }
}
set_error_handler(custom_warning_handler, E_ALL);
function custom_warning_handler($errno, $errstr,$errfile,$errline)
{
     $date = date("d-m-Y H:i");
     $error=$date." ".$errfile." ".$errline." ".$errno." ".$errstr."\r\n";
     error_log($error, 3, "~/DBLogg/Errorlogg.txt"); 
}
?>