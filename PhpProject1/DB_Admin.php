<?php

class DB_Admin{
    
   /*** Variables used to access DB ***/
    
 

    /*** function to allow log inn by admin**/
   
    function logginn(){
            
            $host = "10.10.3.126";
            $user = "maxscale";
            $pw = "mypwd";
            $dbname = "StudentDB";
                   
        
          
            session_start();
            $db = mysqli_connect($host, $user, $pw, $dbname);
            if($db->connect_error){
                die("Could´t connect to database");
            }
            echo $db->host_info;
            $checkPassword = $db->real_escape_string($_POST["password"]);
            $usrnm = $db->real_escape_string($_POST["username"]);
            $sql = "Select * From Admin Where AdminID ='$usrnm'";
            $resultat = $db->query($sql);
            $_SESSION["signedin"]=false;
            if($db->affected_rows>=1){
                $rad = $resultat->fetch_object();
                $salt = $rad->salt;
                
            }
            else{
                echo "Could not connect to password";
            }
            
            $hash = hash("sha1",$salt.$checkPassword);
            //$hash = hash("sha1",$checkPassword);
            $sql = "Select Password From Admin Where AdminID='$username'";
            $resultat = $db->query($sql);
            if($db->affected_rows>=1){
                $rad = $resultat->fetch_object();
                $passordHash = $rad->Passord;
                if($hash == $passordHash){
                    
                    $_SESSION["signedin"]=true;
                    header("Location:AdminIndex.php");
                }
                else{
                    echo "Password or username did not match";
                    
                }
            }
            else{
                
                echo"Could´t find Admin ID";
            }
        
    }
    
    /** shows and writes list of students****/
    
    function showStudents(){
            $host = "10.10.3.126";
            $username = "maxscale";
            $passwd = "mypwd";
            $dbname = "StudentDB";
            
            $db = mysqli_connect($host, $username, $passwd, $dbname);
        if($db->connect_error){
            echo "Failed to connect to database";
            trigger_error($db->connect_error);
        }
        echo $db->host_info;
        $sql = "SELECT Name, Email, StudentID, Program FROM Person AS p, Student AS s WHERE (p.idPerson = s.idStudent)";
        $result = $db->query($sql);
        if(!$result){
            echo "Error loading data";
            trigger_error($db->error);
        }
        else{
            $numberRows = $db->affected_rows;
            echo "<table border = '1.0'><tr><td>Name</td><td>Email</td><td<StudentID</td><td>Program</td>";
            for($i=0;$i<$numberRows;$i++){
                echo"<tr><td>";
                $row = $result->fetch_object();
                echo $row->name."</td><td>".$row->email."</td><td>".$row->studentnumber."</td><td>".$row->program."</td><td>";
            }
            echo "</tr>";
            echo "</tabel>";
        }
        $db->close();
    }
    
    /**** Adds a new student *****/
    
    function addStudent($person, $student){
          $host = "10.10.3.126";
          $username = "maxscale";
          $passwd = "mypwd";
          $dbname = "StudentDB";
          
        $name = $person->get_name();
        $email = $person->get_email();
        $studentID = $student->get_id();
        $program = $student->get_program();
        
        $db = new mysqli($host, $username, $passwd, $dbname);
        if($db->connect_error){
            echo "Error connecting to databse";
            trigger_error($db->connect_error);
        }
        echo $db->host_info;
        $db->autocommit(FALSE);
        $ok = true;
        $sql = "Insert into Person(name,email)";
        $sql .= "Values ($name, $email)";
        $result = $db->query($sql);
        if(!$result){  
            echo"Error writing to database!<br/>";
            trigger_error($db->error);
            $ok=false;
        }
        else{
            $numbRows = $db->affected_rows;
            if($numbRows==0){
                trigger_error("Insert returns 0 rows");
                echo "Could not write to database";
                $ok = false;
            }
        }
        $sql = "Insert into Student(idStudent,Program, StudentID";
        $sql .= "Values(LAST_INSERT_ID(),'$program','$studentID')";
        $result = $db->query($sql);
        if(!$result){  
            echo"Error writing to database!<br/>";
            trigger_error($db->error);
            $ok=false;
        }
        else{
            $numbRows = $db->affected_rows;
            if($numbRows==0){
                trigger_error("Insert returns 0 rows");
                echo "Could not write to database";
                $ok=false;
            }
        }
        if($ok){
            $db->commit();
        }
        else{
            $db->rollback();
        }
        $db->close();
     
    }
    
    /*** Updates Student information ***/
    
    function updateStudent($name, $email,$studentID, $program){
            $host = "10.10.3.126";
            $username = "maxscale";
            $passwd = "mypwd";
            $dbname = "StudentDB";
            
        $db = new mysqli($host, $username, $passwd, $dbname);
        if($db->connect_error){
            echo "Error connecting to databse";
            trigger_error($db->connect_error);
        }
        echo $db->host_info;
        $db->autocommit(FALSE);
        $ok = true;
        $sql = "UPDATE  Person AS p, Student AS s SET Name = '$name', Email = '$email', Program = '$program' WHERE ( StudentID=$studentID AND s.idStudent = p.idPerson";
        $resultat = $db->query($sql);
        if(!$resultat){  
            echo"Error writing to database!<br/>";
            trigger_error($db->error);
            $ok=false;
        }
        else{
            $antallRader = $db->affected_rows;
            if($antallRader==0){
                trigger_error("Insert returns 0 rows");
                echo "Could not write to database";
                $ok=false;
            }
        }
         if($ok){
            $db->commit();
        }
        else{
            $db->rollback();
        }
        $db->close();
        }
        
        
        /*** Deletes Student from Database ***/
   
    function deleteStudent($studentID){
        $host = "10.10.3.126";
        $username = "maxscale";
        $passwd = "mypwd";
        $dbname = "StudentDB";
        $db = new mysqli($host, $username, $passwd, $dbname);
        if($db->connect_error){
            trigger_error($db->connect_error);
            return "Error connecting to databse";
            
        }
        echo $db->host_info;
        $db->autocommit(FALSE);
        $ok = true;
        $sql= "DELETE Person, Student FROM Person INNER JOIN Student WHERE Person.idPerson = Student.idStudent AND Student.StudentID = '$studentID'";
        $resultat = $db->query($sql);
        if(!$resultat){  
            echo"Error writing to database!<br/>";
            trigger_error($db->error);
            $ok=false;
        }
        else{
            $antallRader = $db->affected_rows;
            if($antallRader==0){
                trigger_error("Insert returns 0 rows");
                echo "Could not write to database";
                $ok=false;
            }
        }
          if($ok){
            $db->commit();
        }
        else{
            $db->rollback();
        }
        $db->close();
    
    }
    
        /*** Find Student ***/
    function findStudent($studentID){
        $host = "10.10.3.126";
        $username = "maxscale";
        $passwd = "mypwd";
        $dbname = "StudentDB";
        
        $db = new mysqli($host, $username, $passwd, $dbname);
        if($db->connect_error){
              trigger_error($db->connect_error);
            echo "Error connecting to databse";  
        }
        echo $db->host_info;
        $sql = "SELECT Name, Email, StudentID, Program FROM Student as s, Person as p WHERE (s.idStudent = p.idPerson AND s.StudentID = '$studentID'";
        $result = $db->query($sql);
        if($result->num_rows > 0){
            while($row = $result->fetch_assoc()){
                echo $_POST["updateName"].$row['Name'].$_POST["updateEmail"].$row['Email'].$_POST['updateStudentID'].$row['StudentID'].$_POST['updateProgram'].$row['Pogram'];
            }
        }
        else{
            echo "Could not find Studetn</br>";
        }
        
        
    }
    
    /*** Create new Admin ***/
    function lagreAdmin($person, $admin){
        $host = "10.10.3.126";
        $username = "maxscale";
        $passwd = "mypwd";
        $dbname = "StudentDB";
        
        $name = $person->get_name();
        $email = $person->get_email();
        $password = $admin->get_admin_password();
        $adminID = $admin->get_admin_id();
    
        $db = new mysqli($host, $username, $passwd, $dbname);
        if($db->connect_error){
            echo "Error connecting to databse";
            trigger_error($db->connect_error);
        }
        echo $db->host_info;
        $db->autocommit(FALSE);
        $ok=true;
        $sql = "Insert into Person(Name, Email)";
        $sql.="Values ('$name,'$email')";
        $result = $db->query($sql);
        if(!$result){
            echo"error connecting to database !<br/>";
            trigger_error($db->error);
        }
        else{
            $antallRader = $db->affected_rows;
            if($antallRader==0){
                trigger_error("Insert returns 0 rows");
                echo "could not insert into database";
            }
        }
        function createSalt($length){
            $salt = "";
            $string ="1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOASDFGHJKLZXCVBNM;:,!#$%&";
            $stringlength = strlen($string);
            for($i = 0; $i<$length; $i++){
                $numb = rand(0,$stringlength);
                $sign = $string[$numb];
                $salt.=$sign;
            }
            return $salt;
        }
        $salt = createSalt(20);
        $hash = hash("sha1",$salt.$password);
        $sql = "Insert into Admin (idAdmin, Password, AdminID, Salt)";
        $sql.= "Values(LAST_INSERT_ID(),'$hash','$adminID','$salt')";
        $resultat=$db->query($sql);
        if(!$resultat){
            echo"Error writing to DB";
            trigger_error($db->error);
            $ok=false;
        }
        else{
            $antallRader = $db->affected_rows;
            if($antallRader==0){
                trigger_error("Insert return 0 rows");
                echo "Could not write to DB";
                $ok=false;
            }
        }
        if($ok){
            $db->commit();
        }
        else{
            $db->rollback();
        }
        $db->close();
    }
    
   
}


